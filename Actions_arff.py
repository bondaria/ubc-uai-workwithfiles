'''
@author: Daria

This file creates .arff file from .csv fityj features. Structure of .csv:
Line 0 - list of features
Line 1 - ... - Values of features (each line for each participant) 

'''

Features = ["DurationInteractionDay2","TimeSpentWithContentOverall","TotalTimeWithContent ","TimePageRelSG","TimePageIrrelSG",
            "ProportionTimeRelSGOverReading","ProportionTimeIrrelSGOverReading","TotalTimeWithRelTextContent",
            "TotalTimeWithRelFullContent","TotalTimeWithIrrelTextContent","TotalTimeWithIrrelFullContent",
            "NumberSG","TotalTimeSettingSG","AverageTimeSettingSG","NoteTakingDuration","NoteCheckingDuration",
            "NoteTakingNum","NoteCheckingNum","SummaryAddedNumber","TimePaperNotes","TimeINF","TimePKA","TimeSummary",
            "SGValidated","SGAttempted","FlagWorkedWithoutSubgoal","SGChanged","RatioPagesRelevantToInitialSubgoal1ReadWhenSubgoal1Active",
            "RatioPagesRelevantToInitialSubgoal2ReadWhenSubgoal2Active","RatioPagesRelevantToInitialSubgoal1ReadAtAnytime",
            "RatioPagesRelevantToInitialSubgoal2ReadAtAnytime","numPLAN","numSUMM","numTN","numMPTG","numPKA","numJOL","numFOK","numCE",
            "numINF","AVGNumSRLperPagetoSG0Active","AVGNumSRLpePagetoSG0AnyTime","AVGNumSRLpePagetoSG1AnyTime","TimeFullView_total",
            "ImagesOpened_num","TimeFullView_AVG","FullView_num","TimePage_num"]
#removed correlated 0.7
Features = ["DurationInteractionDay2",
            "ProportionTimeRelSGOverReading","TotalTimeWithRelTextContent",
            "TotalTimeWithRelFullContent","TotalTimeWithIrrelTextContent","TotalTimeWithIrrelFullContent",
            "TotalTimeSettingSG","NoteTakingDuration","NoteCheckingDuration",
            "SummaryAddedNumber","TimePaperNotes","TimeINF","TimePKA","TimeSummary",
            "RatioPagesRelevantToInitialSubgoal1ReadWhenSubgoal1Active",
            "RatioPagesRelevantToInitialSubgoal2ReadWhenSubgoal2Active","RatioPagesRelevantToInitialSubgoal1ReadAtAnytime",
            "numPLAN","numJOL","numFOK","numCE",
            "AVGNumSRLpePagetoSG0AnyTime","AVGNumSRLpePagetoSG1AnyTime","TimeFullView_total",
            "ImagesOpened_num","TimePage_num"]

#removed correlated >=0.9

ActionFeatures = ["DurationInteractionDay2","TimeSpentWithContentOverall","TotalTimeWithContent ","TimePageRelSG","TimePageIrrelSG",
            "TotalTimeWithRelTextContent",
            "TotalTimeWithRelFullContent","TotalTimeWithIrrelTextContent","TotalTimeWithIrrelFullContent",
            "NumberSG","TotalTimeSettingSG","NoteTakingDuration","NoteCheckingDuration",
            "NoteCheckingNum","SummaryAddedNumber","TimePaperNotes","TimeINF","TimePKA","TimeSummary",
            "FlagWorkedWithoutSubgoal","RatioPagesRelevantToInitialSubgoal1ReadWhenSubgoal1Active",
            "RatioPagesRelevantToInitialSubgoal2ReadWhenSubgoal2Active","RatioPagesRelevantToInitialSubgoal1ReadAtAnytime",
            "RatioPagesRelevantToInitialSubgoal2ReadAtAnytime","numPLAN","numSUMM","numMPTG","numPKA","numJOL","numFOK","numCE",
            "numINF","AVGNumSRLpePagetoSG0AnyTime","AVGNumSRLpePagetoSG1AnyTime","TimeFullView_total",
            "ImagesOpened_num","TimePage_num"]

GazeFeatures = ["fixationrate", "meanabspathangles", "meanfixationduration", "meanpathdistance", "meanrelpathangles", "numfixations", 
                "stddevabspathangles", "stddevfixationduration", "stddevpathdistance", "stddevrelpathangles", "OverallLearningGoal_fixationrate", 
                "OverallLearningGoal_longestfixation", "OverallLearningGoal_proportionnum_dynamic", "OverallLearningGoal_proportiontime_dynamic", 
                "OverallLearningGoal_proptransfrom_Agent", "OverallLearningGoal_proptransfrom_LearningStrategiesPalette",
                "OverallLearningGoal_proptransfrom_OverallLearningGoal", "OverallLearningGoal_proptransfrom_Subgoals",
                "OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_fixationrate", "Subgoals_longestfixation", 
                "Subgoals_proportionnum_dynamic", "Subgoals_proportiontime_dynamic", "Subgoals_proptransfrom_Agent", 
                "Subgoals_proptransfrom_LearningStrategiesPalette", "Subgoals_proptransfrom_OverallLearningGoal", "Subgoals_proptransfrom_Subgoals",
                "Subgoals_proptransfrom_TableOfContents", "LearningStrategiesPalette_fixationrate", "LearningStrategiesPalette_longestfixation", 
                "LearningStrategiesPalette_proportionnum_dynamic", "LearningStrategiesPalette_proportiontime_dynamic", 
                "LearningStrategiesPalette_proptransfrom_Agent", "LearningStrategiesPalette_proptransfrom_LearningStrategiesPalette", 
                "LearningStrategiesPalette_proptransfrom_OverallLearningGoal", "LearningStrategiesPalette_proptransfrom_Subgoals",
                "LearningStrategiesPalette_proptransfrom_TableOfContents", "Agent_fixationrate", "Agent_longestfixation", "Agent_proportionnum_dynamic", 
                "Agent_proportiontime_dynamic", "Agent_proptransfrom_Agent", "Agent_proptransfrom_LearningStrategiesPalette", "Agent_proptransfrom_OverallLearningGoal",
                "Agent_proptransfrom_Subgoals", "Agent_proptransfrom_TableOfContents", "TableOfContents_fixationrate", "TableOfContents_longestfixation", 
                "TableOfContents_proportionnum_dynamic", "TableOfContents_proportiontime_dynamic", "TableOfContents_proptransfrom_Agent", 
                "TableOfContents_proptransfrom_LearningStrategiesPalette", "TableOfContents_proptransfrom_OverallLearningGoal", "TableOfContents_proptransfrom_Subgoals",
                "TableOfContents_proptransfrom_TableOfContents", "OverallLearningGoal_proptransfrom_ImageContent", "OverallLearningGoal_proptransfrom_TextContent",
                "Subgoals_proptransfrom_ImageContent", "Subgoals_proptransfrom_TextContent", "LearningStrategiesPalette_proptransfrom_ImageContent", "LearningStrategiesPalette_proptransfrom_TextContent", 
                "TextContent_fixationrate", "TextContent_longestfixation", "TextContent_proportionnum_dynamic", "TextContent_proportiontime_dynamic", "TextContent_proptransfrom_Agent", 
                "TextContent_proptransfrom_ImageContent", "TextContent_proptransfrom_LearningStrategiesPalette", "TextContent_proptransfrom_OverallLearningGoal", 
                "TextContent_proptransfrom_Subgoals", "TextContent_proptransfrom_TableOfContents", "TextContent_proptransfrom_TextContent", "Agent_proptransfrom_ImageContent", 
                "Agent_proptransfrom_TextContent", "ImageContent_fixationrate", "ImageContent_longestfixation", "ImageContent_proportionnum_dynamic", "ImageContent_proportiontime_dynamic", 
                "ImageContent_proptransfrom_Agent", "ImageContent_proptransfrom_ImageContent", "ImageContent_proptransfrom_LearningStrategiesPalette", "ImageContent_proptransfrom_OverallLearningGoal", 
                "ImageContent_proptransfrom_Subgoals", "ImageContent_proptransfrom_TableOfContents", "ImageContent_proptransfrom_TextContent", "TableOfContents_proptransfrom_ImageContent", 
                "TableOfContents_proptransfrom_TextContent"]
Features = ActionFeatures + GazeFeatures

# remove correlated gaze features:
features_correlated = ["meanfixationduration", "meanpathdistance", "OverallLearningGoal_proportiontime_dynamic", "Subgoals_proportiontime_dynamic", "LearningStrategiesPalette_proportiontime_dynamic", "Agent_proportiontime_dynamic", "Agent_proptransfrom_TableOfContents", "TableOfContents_proportiontime_dynamic", "TextContent_proportiontime_dynamic", "TextContent_proptransfrom_TextContent", "ImageContent_proportiontime_dynamic", "ImageContent_proptransfrom_TextContent", "TableOfContents_proptransfrom_TextContent"]
for el in features_correlated:
    Features.remove(el)

#
#laptop:
#DIR = "D:\\Studies\\UAI\\Actions\\work_folder\\"
#Genova:
#
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Actions\\work_folder\\"
infile = DIR + "ActionFeatures_AVG_50_ID.csv" #action features only
infile = DIR + "Final_Full_50p_124f.CSV" #full features
classesfile = DIR + "Participants_Classes.txt"
conditionfile = DIR + "Participants_Conditions.txt"
learningGainsfile = DIR + "Participants_LearningGains.txt"
pageimage = DIR + "ImagesPages.txt"
outfile = DIR + "FullFeatures_111f_50p.arff"
ImgPagesfile = DIR + ""
Participants = [] #List of participants that are considered
dictFeatures = {} # dictionary with PArticipant ID as a key and dictionary of features as value. Dictionary of features contains name of feature as key and its value as value

#process class files with LG:
with open(classesfile, "r") as f:
    lines = f.readlines()
for line in lines:
    line_ar = line[:-1].split("\t")
    Participants.append(line_ar[0])
    dictFeatures[line_ar[0]]={}# add to dictionary new participant and create empty dictionary for him
    dictFeatures[line_ar[0]]["PeformanceClass"] = line_ar[1]
print Participants

# with open(conditionfile, "r") as f:
#     lines = f.readlines()
# for line in lines:
#     line_ar = line[:-1].split("\t")
#     if line_ar[0] in dictFeatures.keys():
#         dictFeatures[line_ar[0]]["Condition"] = line_ar[1]
def ReadFileWithFeature(nameoffile, nameoffeature):
    #nameoffile - file with additional features. First column - participant ID, 2,3... - featurevalues
    #nameoffeature - list of features
    with open(nameoffile, "r") as f:
        lines = f.readlines()
    for line in lines:
        line_ar = line[:-1].split("\t")
        if line_ar[0] in dictFeatures.keys():
            for i in xrange(len(line_ar)-1):
                dictFeatures[line_ar[0]][nameoffeature[i]] = line_ar[i+1]
                #dictFeatures[line_ar[0]][nameoffeature] = line_ar[1]
    
#process files with conditions of the study
#ReadFileWithFeature(conditionfile,["Condition"])
#process files with learning gains of the study
#ReadFileWithFeature(learningGainsfile,["LearningGains"])
#process files with number of pages and images
#ReadFileWithFeature(pageimage,["TextPageNumber", "ImagePageNumber"])


#process actions features 
with open(infile, "r") as f:
    lines = f.readlines()
FeatHeaders = lines[0][:-1].split(",") #read all possible feature headers from file with features
for line in lines[1:]:
    line_ar = line[:-1].split(",")
    print "******************      New Participant!       ***********************"
    for i in xrange(len(FeatHeaders)):
        #the following loop iterates over features FeatHeaders
        #if feature is in Predefined list features, it is added to dictionary for all participants
        if FeatHeaders[i] in Features:
                #print line_ar[0]
                #print i
                #print dictFeatures[line_ar[0]]
                dictFeatures[line_ar[0]][FeatHeaders[i]] = line_ar[i]
        else:
            print "Feature ", FeatHeaders[i], "is not in the list"
#create the arff file    
with open(outfile, 'w') as f:
    f.write("@relation MetaTutorActionCorrelation\n\n")
    for feat in Features:
        f.write("@attribute\t" + feat + "\tnumeric\n")
        
    f.write("@attribute\tPerformanceClass\t{LL,HL}\n")
    #f.write("@attribute\tCondition\t{Control,Feedback}\n")
    #f.write("@attribute\tLearningGains\tnumeric\n")
    #f.write("@attribute\tTextPageNumber\tnumeric\n")
    #f.write("@attribute\tImagePageNumber\tnumeric\n")
    f.write ("\n@data\n\n")
    for part in Participants:
        line = ""
        for feat in Features:
            line += dictFeatures[part][feat] + ","
        line += dictFeatures[part]["PeformanceClass"] + "\n"#","
#        line += dictFeatures[part]["Condition"] + ","
#        line += dictFeatures[part]["LearningGains"] + ","
#        line += dictFeatures[part]["TextPageNumber"] + ","
#        line += dictFeatures[part]["ImagePageNumber"] + "\n"
        f.write(line)
'''
with open(infile, "r") as f:
    lines = f.readlines()
#Features = lines[0][:-1].split(",")
#print Features
with open(outfile, 'w') as f:
    f.write("@relation MetaTutorActionCorrelation\n\n")
    for feat in Features:
        f.write("@attribute\t" + feat + "\tnumeric\n")
    f.write ("\n@data\n\n")
    for line in lines[1:]:
        f.write(line)
'''