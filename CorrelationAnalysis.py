'''
Created on Apr 26, 2013

@author: Daria
'''
from numpy import sqrt
from math import fabs
def cast_int(str):
    """a helper method for converting strings to their integer value
    
    Args:
        str: a string containing a number
    
    Returns:
        the integer value of the string given or None if not an integer
    """
    if str!="" and str!=None:
        v = int(str)
    else:
        v = None
    return v
#    try:
#        v = int(str)
#    except ValueError:
#        v = None
#    return v


def cast_float(str):
    """a helper method for converting strings to their float value
    
    Args:
        str: a string containing a number
    
    Returns:
        the float value of the string given or None if not a float
    """
    if str!="" and str!=None:
        v = float(str)
    else:
        v = None
    return v
def ComputeAvg(feat, i):
    sum = 0
    n = len(feat)
    for el in feat:
        sum += el[i]
    return sum * 1.0 / n
def ComputeCovariance(feat, i, j):
    sum = 0
    n = len(feat)
    avgi = ComputeAvg(feat,i)
    avgj = ComputeAvg(feat,j)
    
    for el in feat:
        sum += (el[i] - avgi) * (el[j] - avgj)
    return sum
def ComputeSS(feat, i):
    sum = 0
    n = len(feat)
    avgi = ComputeAvg(feat,i)
    for el in feat:
        sum += (el[i] - avgi) ** 2
    return sum
def ComputeCorrelation(feat, i, j):
    if (ComputeSS(feat, i) == 0) or  (ComputeSS(feat, j) == 0):
        return "N/A"
    else:
        return 1.0 * ComputeCovariance(feat, i, j) / sqrt(ComputeSS(feat, i) * ComputeSS(feat, j))
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Actions\\Correlation Analysis\\"
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\trials and fails\\Actions_Averaged_Over_reading\\"
infile = DIR + "Actions51_AVG.csv"
outfile = DIR + "Actions51_AVG_correlation_table.csv"
separator = ","
featheaders = []
featvalues = []
with open(infile, 'r') as f:
    lines = f.readlines()
featheaders = lines[0][:-1].split(separator)
for line in lines[1:]:
    arline = line[:-1].split(separator)
    cur = []
    for el in arline:
        cur.append(cast_float(el))
    featvalues.append(cur)
corr = []
cur = []
for i in xrange(len(featvalues[1])):
    cur=[]
    for j in xrange(len(featvalues[1])):
        corr_ij = ComputeCorrelation(featvalues, i, j)
        if (i < j):
            if fabs(corr_ij) > 0.7:
                print featheaders[i], " and ", featheaders[j], " correlated: ", corr_ij
        cur.append(corr_ij)
    corr.append(cur)
print len(corr)
print len(corr[0])
print len(featheaders)
result = []
for el in corr:
    cur = ""
    for item in el:
        cur += str(item) + ","
    result.append(cur + "\n")
print featheaders
with open(outfile, 'w') as f:
    f.write("feat," + ",".join(featheaders)+ "\n")
    for i in xrange(len(result)):
        f.write(featheaders[i]+","+result[i])

