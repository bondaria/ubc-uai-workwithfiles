'''
Created on Dec 8, 2012

@author: Daria
'''
import string

DIR = "D:\\Studies\\UAI\\Outputs\\Current_no_bugs\\"
FILEIN = DIR +"main_87f_47p.csv"

FILEFULL = DIR + "main_full_87f_47p.arff"
skills = "LL,HL,LL,LL,HL,LL,LL,LL,HL,LL,LL,HL,HL,HL,HL,LL,LL,HL,HL,HL,HL,HL,HL,HL,HL,LL,LL,HL,HL,HL,LL,LL,HL,LL,LL,HL,LL,HL,LL,LL,HL,LL,HL,LL,HL,LL,LL"


def create_arffs(FILEIN, DIR, skills, relationname = "MetaTutor", iAOI = 10, name_bool = True, separator = ","):
    # Filein - name of file with features
    # DIR - directory of output file
    # relationname name of the project
    # iAOI - index where AOI-related features start
    # name_bool - True if the first column has participant name
    # separator - "csv" or "tsv"
    fin = open(FILEIN,"r")
    ffull = open(DIR + "full_featureset.arff","w")
    fAOI = open(DIR + "AOI_featureset.arff","w")
    fnoAOI = open(DIR + "noAOI_featureset.arff","w")
    
    lines = fin.readlines()
    #write data file name
    ffull.write("@relation\t" + relationname + "\n\n")
    fAOI.write("@relation\t" + relationname + "\n\n")
    fnoAOI.write("@relation\t" + relationname + "\n\n")
    #write attributes
    attributes = lines[0][:-1].split(separator)
    if name_bool:
        start = 1
    else:
        start = 0
    for attribute in attributes[start:]:
        ffull.write("@attribute\t" + attribute+"\tnumeric" + "\n")
    for attribute in attributes[start:start+iAOI]:
        fnoAOI.write("@attribute\t" + attribute+"\tnumeric" + "\n")
    for attribute in attributes[start+iAOI:]:
        fAOI.write("@attribute\t" + attribute+"\tnumeric" + "\n")
    ffull.write("@attribute\tLearningSkills\t{LL,HL}" + "\n\n")
    fAOI.write("@attribute\tLearningSkills\t{LL,HL}" + "\n\n")
    fnoAOI.write("@attribute\tLearningSkills\t{LL,HL}" + "\n\n")
    
    #print features
    ffull.write("@data\n")
    fAOI.write("@data\n")
    fnoAOI.write("@data\n")
    
    for (line, skill) in zip(lines[1:],skills.split(',')):
        temp = line[:-1].split(separator)
        ffull.write(string.join(temp[start:], ',') +","+skill+ '\n')
        fnoAOI.write(string.join(temp[start:start+iAOI], ',')+ "," + skill + '\n')
        fAOI.write(string.join(temp[start+iAOI:], ',') + "," + skill + '\n')
    
    
create_arffs(FILEIN, DIR, skills, relationname = "MetaTutor", iAOI = 10, name_bool = True, separator = ",")
    
    