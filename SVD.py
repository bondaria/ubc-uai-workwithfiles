'''
This file performs SVD and creates new feature files based on results of SVD
Number of features to be exported should be specified in attribute k
Created on Dec 2, 2012

@author: Daria
'''
#FILE = "D:\\Studies\\UAI\\Outputs\\Current\\SVD\\full-featureset-forSVD.csv"
#DIR = "D:\\Studies\\UAI\\Outputs\\Current_no_bugs\\PCA\\"
#FILE = "D:\\Studies\\UAI\\Outputs\\Midnight\\PCA\\94f_for_PCA.csv"
#AIED2013: 47 participants
#skills = "LL,HL,LL,LL,HL,LL,LL,LL,HL,LL,LL,HL,HL,HL,HL,LL,LL,HL,HL,HL,HL,HL,HL,HL,HL,LL,LL,HL,HL,HL,LL,LL,HL,LL,LL,HL,LL,HL,LL,LL,HL,LL,HL,LL,HL,LL,LL"
#summer 2013, 50 participants
DIR = "D:\\Studies\\UAI\\Actions\\PCA\\"
skills = "LL,HL,LL,LL,HL,LL,LL,LL,HL,LL,LL,HL,HL,HL,HL,LL,LL,HL,HL,HL,HL,HL,HL,HL,HL,LL,LL,HL,HL,HL,LL,LL,LL,HL,LL,LL,HL,HL,LL,HL,LL,LL,HL,LL,HL,LL,LL,HL,LL,LL"
FILE = DIR + "ActionFeatures_AVG_50_PCA.csv"
skills_arr = skills.split(",")
from scipy import *
from pylab import *
import numpy as np
def cast_float(str):
    try:
        v = float(str)
    except ValueError:
        v = None
    return v

fin = open(FILE, "r")
lines = fin.readlines()
fin.close()
temp = lines[0].split(",")
ncols = len(temp)
nrows = len(lines)
#read matrix of features
arr = np.zeros((nrows, ncols))
for i in xrange(nrows):
    temp = lines[i][:-1].split(",")
    for j in xrange(ncols):
        arr[i,j]=cast_float(temp[j])
print arr
#normalize data
var = np.var(arr, axis = 0)
mean = np.mean(arr, axis = 0)
norm_features = np.zeros((nrows, ncols))
for i in xrange(nrows):
    for j in xrange(ncols):
        norm_features[i,j]=(arr[i,j]-mean[j])/sqrt(var[j])
        
print norm_features

#U,S,V = np.linalg.svd(norm_features)
U,S,V = np.linalg.svd(norm_features)
Cov = (np.dot(np.transpose(norm_features),norm_features))/nrows
count_LL=0
for el in skills_arr:
    if el =="LL":
        count_LL = count_LL + 1
count_HL = len(skills_arr) - count_LL

#two features
#k = 3
#Z_LL = np.zeros((count_LL,k))
#Z_HL = np.zeros((count_HL,k))
#iLL = 0
#iHL = 0
#Z = dot (U[:,:k], eye(k)*S[:k])
#
#for i in xrange(nrows):
#    if skills_arr[i]=="HL":
#        Z_HL[iHL,:]=Z[i,:]
#        iHL = iHL+1
#    else:
#        Z_LL[iLL,:] = Z[i,:]
#        iLL = iLL+1
#figure(1)
#plot(Z_LL[:,0], Z_LL[:,1], 'ro')
#plot(Z_HL[:,0], Z_HL[:,1],'bo')
#grid()
#figure(2)
#plot(Z[:,0], Z[:,1],'ro')
#grid()
#show()

#export new featureset
k = 5

Z = dot (U[:,:k], eye(k)*S[:k])
#outfile = "D:\\Studies\\UAI\\Outputs\\Current\\SVD\\new-features-50.csv"
#listoffeatures =  "D:\\Studies\\UAI\\Outputs\\Current\\SVD\\new-attributes-50.arff"
arfffile = DIR + "PCA_"+str(k)+"_features.arff"
outfile = DIR + "new-features-"+str(k)+".csv"
listoffeatures =  DIR + "new-attributes-"+str(k)+".arff"

fnames = ""
for i in xrange(k):
    fnames = fnames + "Feature"+ str(i)+","
fnames = fnames[:-1]+",LearningSkills\n"
with open(outfile, 'w') as f:
        print fnames
        f.write(fnames)
        for (row,skill) in zip(Z,skills_arr):
            line = ""
            for el in row:
                line = line + str(el)+","
            line = line[:-1]+","+skill+"\n"
            #print line
            f.write(line)
with open(listoffeatures, 'w') as ff:
    for el in fnames[:-1].split(',')[:-1]:
        line = "@attribute\t"+el+"\tnumeric\n"
        ff.write(line)
    ff.write("@attribute\tLearningSKills\t{LL,HL}\n")
ff.close()


#create final arff file
with open(arfffile, 'w') as ff:
    ff.write("@relation MetaTutorAction_PCA\n\n")
    
    for el in fnames[:-1].split(',')[:-1]:
        line = "@attribute\t"+el+"\tnumeric\n"
        ff.write(line)
    ff.write("@attribute\tLearningSKills\t{LL,HL}\n")
    ff.write("\n@data\n\n")
    for (row,skill) in zip(Z,skills_arr):
        line = ""
        for el in row:
            line = line + str(el)+","
        line = line[:-1]+","+skill+"\n"
        ff.write(line)