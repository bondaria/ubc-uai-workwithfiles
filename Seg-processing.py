'''
This file is needed to analyze segments files due to overtime accuracy 
Specifically: when exactly participant started browsing

Created on Jan 22, 2013

@author: Daria
'''
DIR = "D:\\Studies\\UAI\\McGill Project Data - structurized\\Segments-main\\"
DIROFFSET = "D:\\Studies\\UAI\\McGill Project Data - structurized\\SEGS\\"
import os
listing = os.listdir(DIR)
listing = filter(lambda y: y[-5:] == '.segs', listing)
#list_of_participants = []
#for l in listing:
#    list_of_participants.append(l[0:len(l)-5]) 

#fout = open(DIR + "output.txt", "w")
#for file in listing:
#    with open(DIR + file, 'r') as f:
#        with open(DIROFFSET + file, 'r') as foffset:
#            lines = f.readlines()
#            lineoffset = foffset.readline()[:-1]
#            print lineoffset
#            if lineoffset.find("seg1")>-1:
#                offset = lineoffset.split("\t")[2] 
#            else:
#                offset = "N/A"
#            start = lines[0].split("\t")[2]
#            end = lines[-1][:-1].split("\t")[3]
#            fout.write(file[:-5]+"\t"+offset+"\t"+start+"\t"+end+"\n")
#            
#fout.close()
fout = open(DIR + "Start-browsing.txt", "w") #this is file with timestamps of when user starts browsing
fout.write("Participant_Id\tabsstarttime\tstarttime\tabstimestamp\ttimestamp\n")
#the key line is 
#"You can now start to browse through the content and..."
searchpattern = "You can now start to browse through the content and"
startpattern = "InputNoContent"
LOGDIR = "D:\\Studies\\UAI\\McGill Project Data - structurized\\LOGS\\"

listing = os.listdir(LOGDIR)
listing = filter(lambda y: y[-4:] == '.log', listing)
print listing
part = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 'MT208PN41061', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 'MT208PN41075', 'MT208PN41076', 'MT208PN41081', 'MT208PN41084', 'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
#Participants =[]
#for l in listing:
#    p = l[6:18]
#    Participants.append(p)
#print Participants
for file in listing:
    with open(LOGDIR + file, 'r') as f:
        lines = f.readlines()
        timestamp = "N/A"
        abstimestamp = "N/A"
        starttime = "N/A"
        startabstime = "N/A"
        for line in lines:
            if line.find (startpattern)>-1 and starttime == "N/A":
                print "We got start"
                startabstime = line.split("\t")[1]
                starttime = line.split("\t")[2]
            if line.find (searchpattern)>-1:
                print "We got search!"
                abstimestamp = line.split("\t")[1]
                timestamp = line.split("\t")[2]
                break
        if (file[6:18] in part):
            fout.write(file[6:18]+"\t"+startabstime+"\t"+starttime+"\t"+abstimestamp+"\t"+timestamp+"\n")
fout.close()