'''
This scripts reads full .csv file of features and creates a new one
THe new file contains only features for participants listed in Participants

Created on May 3, 2013

@author: Daria
'''
Participants = ["MT208PN41005","MT208PN41008","MT208PN41009","MT208PN41010","MT208PN41011","MT208PN41012","MT208PN41013",
                "MT208PN41017","MT208PN41018","MT208PN41019","MT208PN41020","MT208PN41023","MT208PN41024",
                "MT208PN41027","MT208PN41029","MT208PN41030","MT208PN41033","MT208PN41035","MT208PN41037","MT208PN41038",
                "MT208PN41039","MT208PN41040","MT208PN41041","MT208PN41042","MT208PN41043","MT208PN41045","MT208PN41049",
                "MT208PN41050","MT208PN41051","MT208PN41052","MT208PN41053","MT208PN41054","MT208PN41058","MT208PN41059",
                "MT208PN41060","MT208PN41061","MT208PN41064","MT208PN41068","MT208PN41070","MT208PN41072","MT208PN41076",
                "MT208PN41080","MT208PN41081","MT208PN41082","MT208PN41084","MT208PN41085","MT208PN41086","MT208PN41087",
                "MT208PN41088","MT208PN41091"]

DIR = "D:\\Studies\\UAI\\Actions\\data-features\\"
infile = DIR + "ActionFeatures_AVG_66.csv"
outfile = DIR + "ActionFeatures_AVG_50.csv"
with open(infile, 'r') as f:
    lines = f.readlines()
with open(outfile, 'w') as f:
    f.write(lines[0])
    for line in lines[1:]:
        if line.split(',')[0] in Participants:
            print line.split(',')[0]
            f.write(line)
        else:
            print "!!!!!!!!!!!!!!!!!!!!!!!!"
        