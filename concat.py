'''
Created on Nov 30, 2012

@author: Daria
'''
# Thus module concatinates two files:
# - first: .arff file without middle participants
# - second: .csv with remaining participants
import os
#12 out
#full featureset
#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\full12out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\main_47pfull-nomid.arff"
#aoi only
#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\AOI12out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\AOI12out\\main_47pAOI-nomid.arff"
#no AOI

#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\noAOI12out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\noAOI12out\\main_47pnoAOI-nomid.arff"

#9 out
#full feature set

#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\full9out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\full9out\\main_full_9out.arff"

#AOI dependent
#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\AOI9out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\AOI9out\\AOI_full_9out.arff"

#SUFFIX = "AOI"

#AOI-independent

#DIR ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\noAOI9out\\"
#THEFILE ="D:\\Studies\\UAI\\Outputs\\Current\\Extreme Case\\noAOI9out\\main_noAOI_9out.arff"

#SUFFIX = "noAOI"

DIR ="D:\\Studies\\UAI\\Outputs\\Midnight\\AOI-12OUT\\"
THEFILE ="D:\\Studies\\UAI\\Outputs\\Midnight\\ecc_12out_AOI.arff"

SUFFIX = "aoi-12out"

fin = open (THEFILE,'r')
main_file_lines = fin.readlines()
fin.close()
listing = os.listdir(DIR)
listing = filter(lambda y: y[0] == '_', listing)
#listing = ["test.csv"]
print listing



for file in listing:
    fout = open(DIR+SUFFIX+file[:-3]+"arff",'w')
    fin = open(DIR + file,"r")
    lines = fin.readlines()
    fout.writelines(main_file_lines+lines[1:])
    fin.close()
    fout.close()