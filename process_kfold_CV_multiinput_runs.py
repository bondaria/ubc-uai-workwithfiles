'''
Created on Dec 4, 2012
@author: Daria

This module parses WEKA output.
The input file:
WEKA .arff output from Experimenter Mode

NOTE! : Current version supports only one model per file. If you need to analyze more than 1 model, create sepatate files for each

  

'''
from __future__ import division
import scipy as sc
class wekaline():
    #this class will process file with layouts
    #
        def __init__(self, actstr):
            """
            this class represents one line of output.
            It stands for 1 fold of 1 run
            """
            if actstr[0] =="'":
                pos_nameOfrelation = actstr[1:].find("'") + 1
                temp  = actstr[pos_nameOfrelation+2:].split(',')
                temp.insert(0, actstr[1:pos_nameOfrelation]) 
            else:
                temp  = actstr.split(',')
            self.curmodel = temp[0]
            self.curfold = int(temp[1])
            self.currun = int(temp[2])
            self.numoftesting = int(temp[8])
            self.numofcorr = int(temp[9])
            self.numofincorr = int(temp[10])
            self.TP = int(temp[30])
            self.TN = int(temp[34])
            self.FP = int(temp[32])
            self.FN = int(temp[36])

def kappa(TP, FP, TN, FN):
    agreements = TP + TN
    total = TP + FP + TN + FN
    ef_P = (TP + FP) * (TP + FN)
    ef_N = (TN + FN) * (TN + FP)
    ef = (float(ef_P + ef_N))/total
    kappa = (float(agreements - ef))/(total - ef)
    return kappa
def precision(TP, FP, TN, FN):
    return TP/(TP+FP)
def recall(TP, FP, TN, FN):
    return (float(TP))/(TP+FN)
def accuracy (corr, incorr):
    return (float(corr))/(corr + incorr)
# input information
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\GENOVA\\Actions\\work_folder\\Actions_37f\\FeatureSelection\\results\\"
#FILENAME = "res_SimpleLogistic_Actions_37f.arff"
#INPUTFILE = DIR + FILENAME
#OPENFILE = "D:\\Studies\\UAI\\Outputs\\Current_no_bugs\\Feature-Selection-noAOI\\Results\\upd_noAOI-Logistic.arff"
#output information

#OUTDIR = "C:\\Users\\bondaria\\Documents\\UAI\GENOVA\\Actions\\work_folder\\Actions_30f\\FeatureSelection\\results\\"
#OUTFILE = DIR + FILENAME[:-5] + ".csv"

def processARun(data):
    """
    This function processes a single run
    of k-fold cross-validation
    
    INPUT: data is a list of raw lines in arff 
    Length of data is equal to k in k-fold crossvalidation
    
    OUTPUT: a dictionary with statistics as keys and and their calaculated values as values
    
    
    """
    crossvalid = {}
    crossvalid['num'] = 0
    crossvalid['numofcorr'] = 0
    crossvalid['numofincorr'] = 0
    crossvalid['TP'] = 0
    crossvalid['TN'] = 0
    crossvalid['FP'] = 0
    crossvalid['FN'] = 0
        
        
    for eldata in data:
        oneline = wekaline(eldata)
        crossvalid['num'] += oneline.numoftesting
        crossvalid['numofcorr'] += oneline.numofcorr
        crossvalid['numofincorr'] += oneline.numofincorr
        crossvalid['TP'] += oneline.TP
        crossvalid['TN'] += oneline.TN
        crossvalid['FP'] += oneline.FP
        crossvalid['FN'] += oneline.FN
    TP = crossvalid['TP']
    TN = crossvalid['TN']
    FP = crossvalid['FP']
    FN = crossvalid['FN']
    #    print "TP: " + str(TP)
    #    print "FP: " + str(FP)
    #    print "TN: " + str(TN)
    #    print "FN: " + str(FN)
    #    print "kappa: " +str(kappa(TP, FP, TN, FN))[:6]
    #    print "precision: " + str(precision(TP, FP, TN, FN))[:6]
    #    print "recall: " + str(recall(TP, FP, TN, FN))[:6]
    #    print "accuracy: " + str(accuracy(TP+TN, FP+FN))[:6]
    crossvalid['kappa']  = kappa(TP, FP, TN, FN)
    crossvalid['precision'] = precision(TP, FP, TN, FN)
    crossvalid['recall'] = recall(TP, FP, TN, FN)
    crossvalid['accuracy'] = accuracy(TP+TN, FP+FN)
    return crossvalid

def calculateStats(input, listofstats = None):
    """
        Current implementation returns the following statistics in specified order: 
        AVG Accuracy
        SD Accuracy
        LL Accuracy
        HL Accuracy
        Kappa
    """
    f = open(input,'r')
    lines = f.readlines()
    f.close()
    print "Processing file: " + input + "\n"
    print "Length of the file:" + str(len(lines))
    
    pos_str = lines[2].find("{")
    tstr = lines[2][pos_str+1:-2]
    temp =tstr.split(',')
    ndatasets = len(temp)
    print "Number of datatsets: " + str(ndatasets)
    
    pos_str = lines[6].find("{")
    tstr = lines[6][pos_str + 1 : -2]
    temp =tstr.split(',')
    nschemes = len(temp)
    print "Number of schemes: " + str(nschemes)
    
    tstr = lines[3][20:-2]
    temp =tstr.split(',')
    nruns = int(temp[len(temp)-1])
    print "Number of runs: " + str(nruns)
    
    tstr = lines[4][21:-2]
    temp = tstr.split(',')
    nfolds = int(temp[len(temp)-1])
    print "Number of folds: " + str(nfolds)
    
    #nmodels = int(len(lines)/(nfolds*nruns)) - old, doesn't work in some cases
    nmodels = ndatasets * nschemes
    print "Number of models in the file: " + str(nmodels)
    nstr=1
    while lines[nstr]!="@data\n":
        nstr+=1
    nstr+=2
    print "Results start at line # " + str(nstr)
    resultlines = []
    for model in xrange(nmodels):
        results = []
        start_of_model = nstr+model * nruns * nfolds
        print "*************"
        print "Current model is: " + str(model)
        print "nruns: " + str(nruns)
        print "nfolds: " + str(nfolds)
        print "nmodels: " + str(nmodels)
        print "Model Start: " + str(start_of_model)
        print "*************"
        modelname = wekaline(lines[start_of_model]).curmodel
        
        #analysis of one run
        #currun = 0
        #[numoftesting, numofcorr, numofincorr, TP, FP, TN, FN]
        #calculate all statisticsfor all runs and put them to the list "results"
        for currun in xrange(nruns):
            i_start = start_of_model + currun*10
            i_end = i_start + nfolds
            crossvalid = processARun(lines[i_start : i_end])
            results.append(crossvalid)
        
        #calculate final_accuracy:
        final_acc = 0
        acc_fold = []
        for el in results:
            final_acc += el['accuracy']
            acc_fold.append(el['accuracy'])
        final_acc = (float(final_acc))/nruns
        print "Final accuracy: "+ str(final_acc*100)[:6]
        print "Accuracies by folds:"
        print acc_fold
        #High Learners:
        print "Low Learners (LL) accuracy:"
        #for el in results:
        #    print str(float(el["TP"])/(el["TP"] + el["FN"]))
        list_LL_acc = map(lambda el:float(el["TP"])/(el["TP"] + el["FN"]), results)
        print list_LL_acc
        #Low Learners
        print "High Learners (HL) accuracy:"
        list_HL_acc = map(lambda el:float(el["TN"])/(el["TN"] + el["FP"]), results)
        print list_HL_acc
        
        #calculate variance:
        MSE = 0
        for el in results:
            MSE += (el['accuracy']-final_acc) ** 2
        final_var = sc.math.sqrt((float(MSE))/nruns)
        print "Variance: " + str(final_var*100)[:6]
        #calculate accuracy per class
        acc_P = 0
        acc_N = 0
        total_P = 0
        total_N=0
        for el in results:
            acc_P += el["TP"]
            acc_N += el['TN']
            total_P += el["TP"] + el["FN"]
            total_N += el["TN"] + el["FP"]
        LL_acc = float(acc_P)/total_P
        HL_acc = float(acc_N)/total_N
        print "Positive (LL) class accuracy: "+str(float(acc_P)/total_P)
        print "Negative (HL) class accuracy: "+str(float(acc_N)/total_N)
        
        #calculate average kappa
        final_kappa = 0
        kappas = []
        for el in results:
            #print el['kappa']
            final_kappa += el['kappa']
            kappas.append(el['kappa'])
        final_kappa = (float(final_kappa))/nruns
        print "Average kappa: "+ str(final_kappa)[:6]
        print "Kappas by run:"
        print kappas
        line = modelname+ "_" + str(model) + "," + str(final_acc*100)[:6] + "," + str(final_var*100)[:6] + "," + str(LL_acc)[:6] + "," + str(HL_acc)[:6] + "," + str(final_kappa)[:6] +","
        line = line + str(acc_fold)[1:-1] +"," + str(kappas)[1:-1] + "," + str(list_LL_acc)[1:-1] +  "," + str(list_HL_acc)[1:-1] + "\n"
        resultlines.append(line)
    return resultlines, nruns
#DIR - directory with WEKA outputs in .arff format. 
#DIR  = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Full\\results\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Actions\\work_folder\\Actions_37f\\FeatureSelection\\results\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Gaze\\result\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Gaze\\result\\74f_results\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Full\\results\\Full_105f\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Actions\\work_folder\\Actions_30f_Best1St_only\\results\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\Actions\\work_folder\\Gaze as in AIED\\"
#DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\BackwardSearch\\results\\"
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\ForwardSearch\\results\\test\\"

DIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\trials and fails\\"

DIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\trials and fails\\36Actions\\res\\"

# Find all .arff files in folder
import os
listing = os.listdir(DIR)
listing = filter(lambda y: y[-5:] == '.arff', listing)
for FILENAME in listing:
    OUTFILE = DIR + FILENAME[:-5] + ".csv"
    INPUTFILE = DIR + FILENAME
    with open(OUTFILE, "w") as f:
        #accuracies
        lines, nruns = calculateStats(input = INPUTFILE, listofstats = None)
        #line = FILENAME[:-5] + "," + str(final_acc*100)[:6] + "," + str(final_var*100)[:6] + "," + str(LL_acc)[:6] + "," + str(HL_acc)[:6] + "," + str(final_kappa)[:6] +","
        # printing results
        #headers
        line = "NameOfAlgorithm,AVG Accuracy,SD Accuracy,LL Accuracy,HL Accuracy,Kappa,"
        for i in xrange(nruns):
            line = line + "acc_run_" + str(i+1)+","
        for i in xrange(nruns):
            line = line + "kappa_run_" + str(i+1)+","
        for i in xrange(nruns):
            line = line + "LL_acc_run_" + str(i+1)+","
        for i in xrange(nruns):
            line = line + "HL_acc_run_" + str(i+1)+","
        f.write(line[:-1]+"\n")
        for el in lines:
            f.write(el)
#calculate variance:
#oneline = wekaline(lines[nstr+currun*10+6])
#print "TP: " + str(oneline.TP)
#print "FP: " + str(oneline.FP)
#print "TN: " + str(oneline.TN)
#print "FN: " + str(oneline.FN)
#print "kappa: " +str(kappa(oneline.TP, oneline.FP, oneline.TN, oneline.FN))
#print "precision: " + str(precision(oneline.TP, oneline.FP, oneline.TN, oneline.FN))
#print "recall: " + str(recall(oneline.TP, oneline.FP, oneline.TN, oneline.FN))

            